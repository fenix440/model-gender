## Model-Gender ##

Getter and Setter package for some kind of model gender.

This package is part of a project `Aedart\Model`, visit [https://bitbucket.org/aedart/model](https://bitbucket.org/aedart/model) to learn more about it.

Official sub-package website ([https://bitbucket.org/fenix440/model-gender](https://bitbucket.org/fenix440/model-gender))

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of gender property

## How to install ##

```
#!console

composer require fenix440/model-gender  versionNumber
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##


Provided that you have an interface, e.g. for a person, you can extend the gender aware interface;

```
#!php
<?php
use Fenix440\Model\Gender\Interfaces\GenderAware;

interface IPerson extends GenderAware{

    // ... Remaining interface implementation not shown

}
```

In your class implementation, you simple use one of the gender traits.

```
#!php
<?php
use Fenix440\Model\Gender\Traits\GenderTrait;

class MyPerson implements IPerson {
   
     use GenderTrait;
   
     // ... Remaining implementation not shown...

}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package
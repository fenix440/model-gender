<?php
use Fenix440\Model\Gender\Interfaces\GenderTypes;
use Fenix440\Model\Gender\Traits\GenderTrait;
use Fenix440\Model\Gender\Interfaces\GenderAware;

/**
 * Class GenderTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Gender\Traits\GenderTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class GenderTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Gender\Interfaces\GenderAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Gender\Traits\GenderTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     *
     * @covers  ::setGender
     * @covers  ::getGender
     * @covers  ::getDefaultGender
     * @covers  ::hasGender
     * @covers  ::hasDefaultGender
     * @covers  ::isGenderValid
     */
    public function setAndGetFemaleGender()
    {
        $trait = $this->getTraitMock();
        $gender = GenderTypes::FEMALE;
        $trait->setGender($gender);
        $this->assertSame($gender, $trait->getGender(),'Gender is invalid');
    }

    /**
     * @test
     *
     * @covers  ::setGender
     * @covers  ::getGender
     * @covers  ::getDefaultGender
     * @covers  ::hasGender
     * @covers  ::hasDefaultGender
     * @covers  ::isGenderValid
     */
    public function setAndGetMenGender()
    {
        $trait = $this->getTraitMock();
        $gender = GenderTypes::MALE;
        $trait->setGender($gender);
        $this->assertSame($gender, $trait->getGender(),'Gender is invalid');
    }

    /**
     * @test
     *
     * @covers  ::getDefaultGender
     * @covers  ::getGender
     * @covers  ::hasGender
     * @covers  ::hasDefaultGender
     */
    public function getDefaultGender()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getGender(),'Default gender is not null, gender is set!');
    }

    /**
     * @test
     *
     * @covers  ::setGender
     * @covers  ::isGenderValid
     * @expectedException \Fenix440\Model\Gender\Exceptions\InvalidGenderException
     */
    public function setInvalidStringGender()
    {
        $trait = $this->getTraitMock();
        $gender = "unknown";

        $trait->setGender($gender);
    }


    /**
     * @test
     *
     * @covers  ::setGender
     * @covers  ::isGenderValid
     * @expectedException \Fenix440\Model\Gender\Exceptions\InvalidGenderException
     */
    public function setInvalidIntGender()
    {
        $trait = $this->getTraitMock();
        $gender = 232;

        $trait->setGender($gender);
    }





}
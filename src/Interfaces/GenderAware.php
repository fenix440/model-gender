<?php namespace Fenix440\Model\Gender\Interfaces;
use Fenix440\Model\Gender\Exceptions\InvalidGenderException;

/**
 * Interface GenderAware
 *
 * A component/object must be aware of gender property
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Gender\Interfaces
 */
interface GenderAware
{

    /**
     * Set gender for given component
     *
     * @see GenderTypes
     *
     * @param int $gender Gender for given component
     * @return void
     * @throws InvalidGenderException If gender is invalid
     */
    public function setGender($gender);

    /**
     * Get gender
     * @return int|null
     */
    public function getGender();

    /**
     * Get default gender
     *
     * @return int|null
     */
    public function getDefaultGender();

    /**
     * Validates if gender is valid
     *
     * @param mixed $gender Gender to check
     * @return bool true/false
     */
    public function isGenderValid($gender);

    /**
     * Checks if gender is set
     * @return bool true/false
     */
    public function hasGender();

    /**
     * Checks if default gender is set
     *
     * @return bool true/false
     */
    public function hasDefaultGender();

}
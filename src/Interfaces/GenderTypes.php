<?php namespace Fenix440\Model\Gender\Interfaces; 
/**
 * Interface GenderTypes
 *
 * A component must be aware of gender types
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Gender\Interfaces
*/
interface GenderTypes {

    /**
     * Gender men type
     */
    const MALE = 1;

    /**
     * Gender female type
     */
    const FEMALE = 2;


}
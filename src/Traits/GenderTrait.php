<?php  namespace Fenix440\Model\Gender\Traits;
use Aedart\Validate\Number\IntegerValidator;
use Fenix440\Model\Gender\Exceptions\InvalidGenderException;
use Fenix440\Model\Gender\Interfaces\GenderTypes;

/**
 * Trait GenderTrait
 *
 * @see GenderAware
 *
 * @package      Fenix440\Model\Gender\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait GenderTrait {

    /**
     * Gender
     *
     * @var null|int
     */
    protected $gender=null;

    /**
     * Set gender for given component
     *
     * @see GenderTypes
     *
     * @param int $gender Gender for given component
     * @return void
     * @throws InvalidGenderException If gender is invalid
     */
    public function setGender($gender){
        if(!$this->isGenderValid($gender))
            throw new InvalidGenderException(sprintf('Gender "%s" is invalid',$gender));
        $this->gender=$gender;
    }

    /**
     * Get gender
     * @return int|null
     */
    public function getGender(){
        if(!$this->hasGender() && $this->hasDefaultGender())
            $this->setGender($this->getDefaultGender());
        return $this->gender;
    }

    /**
     * Get default gender
     *
     * @return int|null
     */
    public function getDefaultGender(){
        return null;
    }

    /**
     * Validates if gender is valid
     *
     * @param mixed $gender Gender to check
     * @return bool true/false
     */
    public function isGenderValid($gender){
        $status = false;
        if(!IntegerValidator::isValid($gender))
            return $status;

        switch($gender){
            case GenderTypes::MALE:
                $status = true;
                break;
            case GenderTypes::FEMALE:
                $status = true;
                break;
        }
        return $status;
    }

    /**
     * Checks if gender is set
     * @return bool true/false
     */
    public function hasGender(){
        return (!is_null($this->gender))? true:false;
    }

    /**
     * Checks if default gender is set
     *
     * @return bool true/false
     */
    public function hasDefaultGender(){
        return (!is_null($this->getDefaultGender()))? true:false;
    }

}
<?php  namespace Fenix440\Model\Gender\Exceptions; 
/**
 * Class InvalidGenderException
 *
 * Throws an exception if gender is invalid
 *
 * @package Fenix440\Model\Gender\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class InvalidGenderException extends \InvalidArgumentException{

 

}

 